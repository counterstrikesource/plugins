FROM alpine:latest as prepare_stage

# Author
LABEL maintainer="maxime1907 <maxime1907.dev@gmail.com>"

RUN apk update \
    && apk upgrade \
    && apk add \
            curl \
            unzip \
            git \
            bash

RUN mkdir -p /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/include

# Get sourcemod
RUN curl https://gitlab.com/api/v4/projects/16244980/packages/generic/sourcemod/v1.11/sourcemod-linux-v1.11.zip -L -o sourcemod.zip

RUN unzip sourcemod.zip -d /home/alliedmodders/sourcemod/build/package_plugins/

# Get sourcemod extensions
RUN curl https://gitlab.com/api/v4/projects/26375947/packages/generic/sourcemodexts/v1.11/sourcemodexts-linux-v1.11.zip -L -o sourcemodexts.zip

RUN unzip sourcemodexts.zip -d /home/alliedmodders/sourcemod/build/package_plugins/

WORKDIR /home/git/

# Copy every sourcepawn and include file that we need
RUN git clone https://gitlab.com/counterstrikesource/hlstatsx.git --recursive --depth 1
RUN git clone https://gitlab.com/counterstrikesource/sourcebanspp.git --recursive --depth 1
RUN git clone https://gitlab.com/counterstrikesource/zombiereloaded.git --recursive --depth 1

# Copy to corresponding folders
RUN mkdir -p /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/plugins
COPY . /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/plugins
RUN cp -R /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/include /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/plugins/include
RUN cp /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/spcomp /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/plugins

# ZombieReloaded
WORKDIR /home/git/zombiereloaded

RUN mkdir -p /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/plugins/zombiereloaded/addons/sourcemod/scripting/
RUN bash /home/git/zombiereloaded/updateversion.sh --unofficial
RUN cp /home/git/zombiereloaded/src/* -R /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/plugins/zombiereloaded/addons/sourcemod/scripting/
RUN cp /home/git/zombiereloaded/cstrike/* -R /home/alliedmodders/sourcemod/build/package_plugins/

# Hlstatsx
RUN mkdir -p /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/plugins/hlstatsx/addons/sourcemod/scripting/
RUN cp -R /home/git/hlstatsx/sourcemod/scripting/include/* /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/plugins/include/
RUN cp /home/git/hlstatsx/sourcemod/scripting/hlstatsx.sp /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/plugins/hlstatsx/addons/sourcemod/scripting/
RUN cp /home/git/hlstatsx/sourcemod/scripting/superlogs-css.sp /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/plugins/hlstatsx/addons/sourcemod/scripting/

# Sourcebans++
RUN mkdir -p /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/plugins/sourcebanspp/addons/sourcemod/scripting/
RUN cp -R /home/git/sourcebanspp/game/* /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/plugins/sourcebanspp/

# Copy include files
RUN cp -R /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/plugins/*/addons/sourcemod/scripting/include/* /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/plugins/include

# Prepare output directories
RUN mkdir -p /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/configs/
RUN mkdir -p /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/data/
RUN mkdir -p /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/gamedata/
RUN mkdir -p /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/translations/
RUN mkdir -p /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/plugins/custom/

# Remove discord cfgs because it does a cp conflict
RUN rm /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/plugins/*/addons/sourcemod/configs/discord.cfg
RUN cp -R /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/plugins/*/addons/sourcemod/configs/* /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/configs/

RUN cp -R /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/plugins/*/addons/sourcemod/data/* /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/data/
RUN cp -R /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/plugins/*/addons/sourcemod/gamedata/* /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/gamedata/
RUN cp -R /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/plugins/*/addons/sourcemod/translations/* /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/translations/
RUN cp -R /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/plugins/*/materials/* /home/alliedmodders/sourcemod/build/package_plugins/materials/
RUN cp -R /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/plugins/*/models/* /home/alliedmodders/sourcemod/build/package_plugins/models/
RUN cp -R /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/plugins/*/sound/* /home/alliedmodders/sourcemod/build/package_plugins/sound/
RUN cp -R /home/alliedmodders/sourcemod/build/package_plugins/addons/sourcemod/scripting/plugins/*/cfg/* /home/alliedmodders/sourcemod/build/package_plugins/cfg/

FROM ubuntu:20.04

# Install dependencies for SourceMod
RUN dpkg --add-architecture i386 \
        && apt update \
        && apt upgrade -y \
        && apt install -y \
            lib32stdc++-7-dev \
            python3

COPY --from=prepare_stage /home/alliedmodders/sourcemod/build/package_plugins/ /package_plugins

# Compile plugins
WORKDIR /package_plugins/addons/sourcemod/scripting/plugins

RUN python3 compile-all.py

# Copy plugins data, configs and .smx
RUN cp -R /package_plugins/addons/sourcemod/scripting/plugins/plugins/* /package_plugins/addons/sourcemod/plugins/custom/
